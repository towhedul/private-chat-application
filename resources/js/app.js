

require('./bootstrap');

window.Vue = require('vue');
//Vuex Support
import Vuex from 'vuex'
Vue.use(Vuex)
import storeVuex from './store/index'
const store = new Vuex.Store(storeVuex)

//scroll
import VueChatScroll from 'vue-chat-scroll'
Vue.use(VueChatScroll)

window.moment = require('moment');
Vue.filter('dateFormat', function (value) {
  return moment(value).format('MMMM Do YY, h:mm a');
})





// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('main-app', require('./components/Main.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store
});
