<?php

namespace App\Http\Controllers;


use App\Messahe;
use Illuminate\Http\Request;
use App\User;


class MessaheController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function user_list(){

        $users = User::latest()->where("id","!=",auth()->user()->id)->get();
        if (\Request::ajax()) {
        	return response()->json($users,200);
        }else{
        	return abort(404);
        }
        
    }

    public function user_messsage($id=null){

    	if (!\Request::ajax()) {
            return abort(404);
        }else{
            $user=User::findOrFail($id);
/*
        $messge=Messahe::where(function($q) use($id){
            $q->where('from',auth()->user()->id);
            $q->where('to',$id);
            $q->where('type',0);
        })->orwhere(function($q) use($id){
            $q->where('from',$id);
            $q->where('to',auth()->user()->id);
            $q->where('type',1);
        })->with('user')->get();*/

        $messge = $this->message_by_user_id($id);

        return response()->json([
            'message'=>$messge,
            'user'=>$user
        ],200);
        }

        
    }


    public function send_messsage(Request $request){
         
         if (!$request->ajax()) {
             return abort(404);
         }
         $message=Messahe::create([
           'message'=>$request->message,
           'from'=>auth()->user()->id,
           'to'=>$request->user_id,
           'type'=>0
         ]);
         $message=Messahe::create([
           'message'=>$request->message,
           'from'=>auth()->user()->id,
           'to'=>$request->user_id,
           'type'=>1
         ]);
         return response()->json($message,200);
    }

    public function delete_sin_message($id=null){
        if (!\Request::ajax()) {
            return abort(404);
        }
        $messge=Messahe::findOrFail($id);
        $messge->delete();
    }

    public function delete_all_message($id=null){
       $message=$this->message_by_user_id($id);
       foreach ($message as  $value) {
           Messahe::findOrFail($value->id)->delete();
       }
       return response()->json('All Message Deleted',200);

    }

    public function message_by_user_id($id){
        $messge=Messahe::where(function($q) use($id){
            $q->where('from',auth()->user()->id);
            $q->where('to',$id);
            $q->where('type',0);
        })->orwhere(function($q) use($id){
            $q->where('from',$id);
            $q->where('to',auth()->user()->id);
            $q->where('type',1);
        })->with('user')->get();
        return $messge;
    }
}
