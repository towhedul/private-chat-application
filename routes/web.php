<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('user-list', 'MessaheController@user_list')->name('user.list');
Route::get('user-message/{id}', 'MessaheController@user_messsage')->name('user.message');
Route::post('sendMessage', 'MessaheController@send_messsage')->name('user.message.send');
Route::get('deleteSinMessage/{id}', 'MessaheController@delete_sin_message')->name('user.sinmessage.delete');
Route::get('deleteAllMessage/{id}', 'MessaheController@delete_all_message')->name('user.delmessage.delete');
